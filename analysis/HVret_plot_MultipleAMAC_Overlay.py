########### This script is for 10 iterations for each count ###################
########### AMAC Sl nos 05, 09 and 10 HV Current Monitor output for sw0000 is compared. #############
########## Author: A.N , V.R  - HEP, UPenn		Date: 08/07/2018     ############################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

#NOTE: ALL files that are to be overlaid need to have same number of entries
#To generate histogram for S0000
point1_start = 0			# Dependent on size of data
point1_end = 199
point2_start = 10000
point2_end = 10199
point3_start = 19000
point3_end = 19199

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

# Import data
dac_counts = []
amac_counts_AMAC05 = []
amac_counts_AMAC09 = []
amac_counts_AMAC10 = []

with open('HVret_ramp_AMAC5_trunc.csv', 'rb') as csvfile:	# generated from AMACHVret_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		dac_counts.append(int(column[1])) #DAC entries are same for all three csv. Verify this before running the script.
		amac_counts_AMAC05.append(int(column[2]))
		
with open('HVret_ramp_AMAC9_trunc.csv', 'rb') as csvfile:	# generated from AMACHVret_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		amac_counts_AMAC09.append(int(column[2]))
		
with open('HVret_ramp_AMAC10_trunc.csv', 'rb') as csvfile:	# generated from AMACHVret_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		amac_counts_AMAC10.append(int(column[2]))
		
# Convert DAC counts to equivalent current injection into AMACv2
uA = []
for count in dac_counts:
	uA.append((count*v_per_count*divider)/1000*1e6)
		
# Find fit of AMACv2 counts v. current line; gives gain 
sat_point = 20201
#___________Plot AMAC Sl no 5___________________________________________________________________________________________________________________________
m, b = np.polyfit(uA[0:sat_point], amac_counts_AMAC05[0:sat_point], 1) # data saturates at 1019th entry. 
fitted_amac_counts_AMAC05 = np.multiply(uA, m) + b
nA_per_count = 1.0/m * 1e3
fit_data = "(%.3G nA/count), " %nA_per_count + " VDD: 1.24V"+ " BG = 656 mV, rampgain = 0x3"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(uA, amac_counts_AMAC05, '.')

plt.plot(uA, fitted_amac_counts_AMAC05, '-', color='blue', label="AMAC #5 fit: " + fit_data)

#___________Plot AMAC Sl no 9____________________________________________________________________________________________________________________________
m, b = np.polyfit(uA[0:sat_point], amac_counts_AMAC09[0:sat_point], 1) # data saturates at 1019th entry. 
fitted_amac_counts_AMAC09 = np.multiply(uA, m) + b
nA_per_count = 1.0/m * 1e3
fit_data = "(%.3G nA/count), " %nA_per_count + " VDD: 1.25V"+ " BG = 672 mV, rampgain = 0x1"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(uA, amac_counts_AMAC09, '.')
plt.plot(uA, fitted_amac_counts_AMAC09, '-', color='green', label="AMAC #9 fit: " + fit_data)

#___________ Plot AMAC Sl no 10_________________________________________________________________________________________________________________________
m, b = np.polyfit(uA[0:sat_point], amac_counts_AMAC10[0:sat_point], 1) # data saturates at 1019th entry. 
fitted_amac_counts_AMAC10 = np.multiply(uA, m) + b
nA_per_count = 1.0/m * 1e3
fit_data = "(%.3G nA/count), " %nA_per_count + " VDD: 1.23V"+ " BG = 658mV, rampgain = 0x1"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(uA, amac_counts_AMAC10, '.')
plt.plot(uA, fitted_amac_counts_AMAC10, '-', color='red', label="AMAC #10 fit: " + fit_data)

#___________________ Set Axis for the plot________________________________________________________________________________________________________________
plt.xlabel('Test current (uA)')
plt.ylabel('AMAC counts')
plt.legend(frameon=True,ncol=1, loc=4, fontsize=14)
plt.title("Preliminary HVret Measurement \n Comparing AMACv2 #5, #9 and #10 @ room temp. \n Overlay of 200 measurements, DAC steps of %.3G nA" %dac_steps_in_nA)

'''# Plot distributions of a few select points
mean_current = nA_per_count * (np.mean(amac_counts[point1_start:point1_end]) - offset_current) / 1000
std_current = nA_per_count * (np.std(amac_counts[point1_start:point1_end]))
plt.subplot(3,3,7)
plt.hist(amac_counts[point1_start:point1_end], label = "mean = %.3G uA" %mean_current + "\nsigma = %.3G nA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt1 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt1.legendHandles:
	item.set_visible(False)
	
mean_current = nA_per_count * (np.mean(amac_counts[point2_start:point2_end]) - offset_current) / 1000
std_current = nA_per_count * (np.std(amac_counts[point2_start:point2_end]))
plt.subplot(3,3,8)
plt.hist(amac_counts[point2_start:point2_end], label = "mean = %.3G uA" %mean_current + "\nsigma = %.3G nA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt2 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt2.legendHandles:
	item.set_visible(False)

mean_current = nA_per_count * (np.mean(amac_counts[point3_start:point3_end]) - offset_current) / 1000
std_current = nA_per_count * (np.std(amac_counts[point3_start:point3_end]))
plt.subplot(3,3,9)
plt.hist(amac_counts[point3_start:point3_end], label = "mean = %.3G uA" %mean_current + "\nsigma = %.3G nA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt3 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt3.legendHandles:
	item.set_visible(False)'''

plt.show()
#plt.savefig('HVret.pdf')
