import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

filename = 'HVret_ramp_AMAC5.csv'
amac_num = 5				# serial number
VDD = 1.24					# in V, determined via calibration
bgmV = 656					# in mV

point1_start = 0			# Dependent on size of data
point1_end = 199
point2_start = 10000
point2_end = 10199
point3_start = 19000
point3_end = 19199

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

# Import data
dac_counts = []
amac_counts = []
with open(filename, 'rb') as csvfile:	# generated from AMACHVret_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		dac_counts.append(int(column[1]))
		amac_counts.append(int(column[2]))
				
# Convert DAC counts to equivalent current injection into AMACv2
uA = []
for count in dac_counts:
	uA.append((count*v_per_count*divider)/1000*1e6)
		
# Find fit of AMACv2 counts v. current line; gives gain 
m, b = np.polyfit(uA, amac_counts, 1)
fitted_amac_counts = np.multiply(uA, m) + b
nA_per_count = 1.0/m * 1e3
fit_data = "m = %.3G count/uA " %m + "(%.3G nA/count), " %nA_per_count + "b = %.3G counts" %b

offset_current = np.mean(amac_counts[point1_start:point1_end])	# mean of the first point (offset)

# Plot overlayed data
plt.subplot(3,3,(1,6))
plt.plot(uA, amac_counts, '.')
plt.plot(uA, fitted_amac_counts, '-', label="fit: " + fit_data)
plt.xlabel('Test current (uA)')
plt.ylabel('AMAC counts')
plt.legend(frameon=False,ncol=2)
plt.title("Preliminary HVret Measurement \n AMACv2 #%d, VDD=%.3GV, BG600=%dmV, \n Overlay of 200 measurements, DAC steps of %.3G nA" %(amac_num, VDD, bgmV, dac_steps_in_nA))

# Plot distributions of a few select points
mean_current = (np.mean(amac_counts[point1_start:point1_end]))
std_current = (np.std(amac_counts[point1_start:point1_end]))
plt.subplot(3,3,7)
plt.hist(amac_counts[point1_start:point1_end], label = "mean = %.3G counts" %mean_current + "\nsigma = %.3G counts " %std_current + "\n        (%.3G nA)" % (std_current * nA_per_count))
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt1 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt1.legendHandles:
	item.set_visible(False)
	
mean_current = (np.mean(amac_counts[point2_start:point2_end]))
std_current = (np.std(amac_counts[point2_start:point2_end]))
plt.subplot(3,3,8)
plt.hist(amac_counts[point2_start:point2_end], label = "mean = %.3G counts" %mean_current + "\nsigma = %.3G counts " %std_current + "\n        (%.3G nA)" % (std_current * nA_per_count))
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt2 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt2.legendHandles:
	item.set_visible(False)

mean_current = (np.mean(amac_counts[point3_start:point3_end]))
std_current = (np.std(amac_counts[point3_start:point3_end]))
plt.subplot(3,3,9)
plt.hist(amac_counts[point3_start:point3_end], label = "mean = %.3G counts" %mean_current + "\nsigma = %.3G counts " %std_current + "\n        (%.3G nA)" % (std_current * nA_per_count))
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt3 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt3.legendHandles:
	item.set_visible(False)

plt.show()
#plt.savefig('HVret.pdf')
