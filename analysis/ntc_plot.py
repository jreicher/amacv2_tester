import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

# Import AMAC data
ohms = []
amac_counts = []
with open('NTCx_sw110.csv', 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		ohms.append(int(column[0]))
		amac_counts.append(int(column[1]))
		
plt.figure()
plt.plot(ohms, amac_counts, '.')
plt.xlabel('Test external resistance (Ohms)')
plt.ylabel('AMAC counts')
plt.legend(frameon=False,ncol=2)
plt.title("Preliminary NTC Measurement, NTCx, switch = 3b110 \n AMACv2 #6, VDD=1.23V, BG600=653mV, rampgain = 0x1")
plt.show()

# Import NTC manufacturer data
# From http://www.semitec.co.jp/uploads/english/sites/2/2017/03/P16-17-KT-Thermistor.pdf
ntc_temp = []
ntc_kohms = []
with open('103KT1608T.csv', 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter=",")
	for column in reader:
		ntc_temp.append(int(column[0]))
		ntc_kohms.append(float(column[1]))
		
# Seventh degree polynomial fit of NTC manufacturer data
# Make the fitted data the same size as the AMAC data,
# so we effectively have a lookup table to go from ohms to temp
poly_degree = 7
coeff = np.polyfit(ntc_temp, ntc_kohms, poly_degree)
ntc_poly = np.poly1d(coeff)
ntc_poly_temp = np.linspace(ntc_temp[0], ntc_temp[-1], len(ohms))
ntc_poly_res = ntc_poly(ntc_poly_temp)

plt.figure()
plt.plot(ntc_temp, ntc_kohms, '.')
plt.plot(ntc_poly_temp, ntc_poly_res, '-')
plt.xlabel('Temperature (C)')
plt.ylabel('NTC resistance (kOhms)')
plt.legend(frameon=False,ncol=2)
plt.title("103KT1608T Resistance/Temperature Characteristic")
plt.show()

# Find AMAC temperature by finding roots of polynomial
# above for given resistance value. Assumption: the temperature
# will always be the real part of the highest degree root - maybe 
# not true! But seems to work ...
amac_temp = []
kohms = np.divide(ohms, 1000)
for res in np.linspace(kohms[0], kohms[-1], len(kohms)):
	all_roots = (ntc_poly - res).roots
	derived_temp = all_roots[-1].real
	amac_temp.append(derived_temp)
	
plt.figure()
plt.plot(amac_temp, amac_counts, '.')
plt.xlabel('Derived Temperature (C)')
plt.ylabel('AMAC counts')
plt.legend(frameon=False,ncol=2)
plt.title("Preliminary NTC Measurement, NTCx, switch = 3b110 \n AMACv2 #6, VDD=1.23V, BG600=653mV, rampgain = 0x1")
plt.show()
