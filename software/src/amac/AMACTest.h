#ifndef AMACTEST_H_
#define AMACTEST_H_

#include <memory>

#include "AMACTB.h"

namespace AMACTime{
  std::string getTime();
}

class AMACTest 
{
public:
  AMACTest(const std::string& name, std::shared_ptr<AMACTB> amactb);
  AMACTest();
  ~AMACTest();

  void runPower();

  void dumpRegisters();

  float runBER(uint trails=10000);  
  void runBERvsClock();
  void runBERvsCounts();

  void runBandgapScan();

  void runVoltageADC(const std::string& chname, AMACv2Field AMACv2Reg::* ch, dac_t dac);
  void runZeroCalib(const std::string& chname, AMACv2Field AMACv2Reg::* ch);

  void runClocks();

  void readADCsAndAMChannels(const std::string& suffix = "");

private:
  std::string m_name;
  
  std::shared_ptr<AMACTB> m_amactb;

};

#endif // AMACTEST_H_
