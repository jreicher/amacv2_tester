#include "AMACTB.h"
#include "EndeavourComException.h"
#include "SPICom.h"
#include "AD5160.h"
#include "ComIOException.h"
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <vector>

void print_help()
{
  std::cout << "usage: potSet [NTCx,NTCy,NTCpb] command [Ohms (50-100000)]" << std::endl;
}

int main(int argc, char* argv[])
{
	AMACTB tb;
	
  	// Determine the command
  	if(argc<=optind)
    	{
      		print_help();
      		return 1;
   	}

  	std::string command=argv[optind];

  	// Run the commands
  	try
  	{
    		if(command=="NTCx")	
    		{
			// Need one more
			if(argc<=(optind+1))
			{
				print_help();
	 			return 1;
			}
	
			float val=std::stoul(argv[optind+1], nullptr, 0);
        		tb.POT2.setResistance(val);
			std::cout << "Setting NTCx pot to: " << val << " Ohms."<< std::endl;
		}
    		else if(command=="NTCpb")
		{
	  		// Need one more
	  		if(argc<=(optind+1))
	    		{
	      			print_help();
	      			return 1;
	    		}

	  		float val=std::stoul(argv[optind+1], nullptr, 0);
      			tb.POT1.setResistance(val);
	  		std::cout << "Setting NTCpb pot to: " << val << " Ohms."<< std::endl;
		}
	    	else if(command=="NTCy")
		{
	  		// Need one more
	  		if(argc<=(optind+1))
	    		{
	      			print_help();
	      			return 1;
	    		}

	  		float val=std::stoul(argv[optind+1], nullptr, 0);
      			tb.POT0.setResistance(val);
	  		std::cout << "Setting NTCy pot to: " << val << " Ohms."<< std::endl;
		}
   	}
  	catch(EndeavourComException e)
    	{
      		std::cout << e.what() << std::endl;
    	}
  return 0;
}

