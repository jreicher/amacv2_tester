/* This program tests the range and slope of the DCDC Input Current Monitor
 * The VDD, AMBG, Ramp Gain values required to get a ramp of 1mV/count are have to be manually for each AMAC in the program.
 * Calibration of the current monitor is manual.
 * Author: V.R.,HEP, UPenn
 * Date: 08/15/2018
 *
 */


#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

#include <EndeavourCom.h>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, 0x898c); // VDD for AMAC #9 (value after ramp calibration)
  TB->END.write_reg(53, 0x01000000);// Choosing AM Int Slope which gave ~ 1mV/count
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);

  TB->END.write_reg(58, 0x00401605);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  uint data;
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  TB->setDAC(TB->Cur10Vp, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V initially to get zero current through "sense resistor".
  TB->setDAC(TB->Cur10Vp_offset, 1.2); // Set Cur10VpOffset to 1.1842 to be 50 mV below Cur10Vp.
  usleep(1E6);

  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  uint ch12_val = (data & 0x000003FF);
  std::cout << "Initial ch 12 value:" << "\t" << ch12_val << std::dec << std::endl;

  
#if 0
  //====================================FOR CALIBRATION=============================================================
  //------------- Read Vlow------------------------------------------------------------------
   TB->END.write_reg(53, 0x01010000);// Choosing AM Int Slope which gave ~ 1mV/count
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);

  TB->END.write_reg(58, 0x00401605);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  TB->setDAC(TB->Cur10Vp, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V initially to get zero current through "sense resistor".
  TB->setDAC(TB->Cur10Vp_offset, 1.2); // Set Cur10VpOffset to 1.1842 to be 50 mV below Cur10Vp.
  usleep(1E6);

  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
 ch12_val = (data & 0x000003FF);
  std::cout << "VlowTP Value" << "\t" << ch12_val << std::dec << std::endl;

  //------------- Read Vhigh------------------------------------------------------------------
   TB->END.write_reg(53, 0x01020000);// Choosing AM Int Slope which gave ~ 1mV/count
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);

  TB->END.write_reg(58, 0x00401605);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  data;
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  TB->setDAC(TB->Cur10Vp, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V initially to get zero current through "sense resistor".
  TB->setDAC(TB->Cur10Vp_offset, 1.2); // Set Cur10VpOffset to 1.1842 to be 50 mV below Cur10Vp.
  usleep(1E6);

  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  ch12_val = (data & 0x000003FF);
 std::cout << "Vhigh TP Value" << "\t" << ch12_val << std::dec << std::endl;


  //=========================================================================================================
  //===========================================END CALIBRATION===============================================
#endif

  std::ofstream fh;
  fh.open("InputCM_test_"+AMACTime::getTime()+".csv");
  fh << "Channel\tCurrent \tInput CM counts\tdatetime" << std::endl;


/* Max current measurement requirement for DC-DC Input Current Monitor is 1A, Rsense series resistor is 50 mOhm. (1 A * 50 mOhm = 50 mV). But, DAC program has a multiplication factor as 3.121. So 1.2V * 3.121 = 3.746 @ DAC channel. Setting Cur10Vp =3.746 V and varying Cur10Vp_Offs
et from 3.746 V to 3.696 V to create 50 mV offset in ~ 1 mV count. */

  for(float Cur10VpOffVal=1.2;Cur10VpOffVal>=1.184;Cur10VpOffVal-=0.00032)
    {
      for(int num_meas=0;num_meas<200;num_meas+=1)
        {
        
        // I = Cur10Vp-Cur10Vp_offset/Rsense
        // Cur10VpOffset is varied to generate I.
        
        TB->setDAC(TB->Cur10Vp, 1.2); //Create Vdrop across Rsense resistor. Set Cur1Vp = 3.6/3 = 1.2 V 
        TB->setDAC(TB->Cur10Vp_offset, Cur10VpOffVal); // Vary Cur1VpOffVal
        usleep(10E4); //100m sec delay
        data=TB->END.read_reg(14);
        std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
        uint ch12_val = (data & 0x000003FF);
        std::cout << "ch 12 value:" << "\t" << ch12_val << std::dec << std::endl;

        float currIn = (((1.2*3.121) - (Cur10VpOffVal*3.121))/(50*(1E-3))); //Calculating equivalent current

        int currIn10E2 = currIn * 10E2;
        std::cout << "rounded 100:" << "\t" << currIn10E2 << std::dec << std::endl;
        float currInRounded = (float) currIn10E2/10E2; 
        fh << 12 << "\t" << currInRounded << "\t" << ch12_val << "\t" << AMACTime::getTime() << std::endl;
        }
    }
  fh.close();

  //Resetting DAC channels
  TB->setDAC(TB->Cur10Vp,0); 
  TB->setDAC(TB->Cur10Vp_offset,0); 


  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down


  return 0;
}
