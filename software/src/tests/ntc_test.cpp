#include "AMACTB.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();

  // Power on
  TB->powerOn();
  usleep(1E6); // 1s wait for a clean power up
  std::cout << "Power ON" << std::endl;
  
  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);
  
  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);
  
  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
  }
  
  // Calibrate AMAC to 1 mV / count
  TB->END.write_reg(52, 0x8a8c); 		// AMAC #6
  std::cout << "Setting VDD and AMBG" << std::endl;
  usleep(1E6);
  TB->END.write_reg(53, 0x01000000);	// AMAC #6
  std::cout << "Setting ramp gain" << std::endl;
  usleep(1E6);

  /*
   * START
   * Run through all NTC range setting, change pot 500   - 100k ohms
   * Save results in separately named files
   */
  TB->END.write_reg(57, 0x00040408); // NTCx CAL bit off, switch = 3b000
  std::cout << "Taking NTC circuit out of CAL mode" << std::endl;
  usleep(1E6);

  std::ofstream fh;
  fh.open("NTCx_sw000_"+AMACTime::getTime()+".csv");
  fh << "Ohms\tAMAC Counts\tdatetime" << std::endl;
  for (float val=500; val <=100000; val +=500)
  {
    TB->POT2.setResistance(val);
    std::cout << "setting NTCx to: " << std::dec << val << " Ohms" << std::endl;
    usleep(1e5);
    unsigned int data12 = TB->END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " has hex data: 0x" << std::hex << data12 << std::endl;
    int ch7 = (data12 & 0x000FFC00) >> 10;
    std::cout << "ch7:  " << std::dec << ch7 << std::endl;
    fh << val << "\t" << ch7 << "\t" << AMACTime::getTime() << std::endl;
  }
  fh.close();

  TB->END.write_reg(57, 0x00040409); // NTCx CAL bit off, switch = 3b001
  std::cout << "Taking NTC circuit out of CAL mode" << std::endl;
  usleep(1E6);

  fh.open("NTCx_sw001_"+AMACTime::getTime()+".csv");
  fh << "Ohms\tAMAC Counts\tdatetime" << std::endl;
  for (float val=500; val <=100000; val +=500)
  {
    TB->POT2.setResistance(val);
    std::cout << "setting NTCx to: " << std::dec << val << " Ohms" << std::endl;
    usleep(1e5);
    unsigned int data12 = TB->END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " has hex data: 0x" << std::hex << data12 << std::endl;
    int ch7 = (data12 & 0x000FFC00) >> 10;
    std::cout << "ch7:  " << std::dec << ch7 << std::endl;
    fh << val << "\t" << ch7 << "\t" << AMACTime::getTime() << std::endl;
  }
  fh.close();

  TB->END.write_reg(57, 0x0004040a); // NTCx CAL bit off, switch = 3b010
  std::cout << "Taking NTC circuit out of CAL mode" << std::endl;
  usleep(1E6);

  fh.open("NTCx_sw010_"+AMACTime::getTime()+".csv");
  fh << "Ohms\tAMAC Counts\tdatetime" << std::endl;
  for (float val=500; val <=100000; val +=500)
  {
    TB->POT2.setResistance(val);
    std::cout << "setting NTCx to: " << std::dec << val << " Ohms" << std::endl;
    usleep(1e5);
    unsigned int data12 = TB->END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " has hex data: 0x" << std::hex << data12 << std::endl;
    int ch7 = (data12 & 0x000FFC00) >> 10;
    std::cout << "ch7:  " << std::dec << ch7 << std::endl;
    fh << val << "\t" << ch7 << "\t" << AMACTime::getTime() << std::endl;
  }
  fh.close();

  TB->END.write_reg(57, 0x0004040c); // NTCx CAL bit off, switch = 3b100
  std::cout << "Taking NTC circuit out of CAL mode" << std::endl;
  usleep(1E6);

  fh.open("NTCx_sw100_"+AMACTime::getTime()+".csv");
  fh << "Ohms\tAMAC Counts\tdatetime" << std::endl;
  for (float val=500; val <=100000; val +=500)
  {
    TB->POT2.setResistance(val);
    std::cout << "setting NTCx to: " << std::dec << val << " Ohms" << std::endl;
    usleep(1e5);
    unsigned int data12 = TB->END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " has hex data: 0x" << std::hex << data12 << std::endl;
    int ch7 = (data12 & 0x000FFC00) >> 10;
    std::cout << "ch7:  " << std::dec << ch7 << std::endl;
    fh << val << "\t" << ch7 << "\t" << AMACTime::getTime() << std::endl;
  }
  fh.close();

  TB->END.write_reg(57, 0x0004040e); // NTCx CAL bit off, switch = 3b110
  std::cout << "Taking NTC circuit out of CAL mode" << std::endl;
  usleep(1E6);

  fh.open("NTCx_sw110_"+AMACTime::getTime()+".csv");
  fh << "Ohms\tAMAC Counts\tdatetime" << std::endl;
  for (float val=500; val <=100000; val +=500)
  {
    TB->POT2.setResistance(val);
    std::cout << "setting NTCx to: " << std::dec << val << " Ohms" << std::endl;
    usleep(1e5);
    unsigned int data12 = TB->END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " has hex data: 0x" << std::hex << data12 << std::endl;
    int ch7 = (data12 & 0x000FFC00) >> 10;
    std::cout << "ch7:  " << std::dec << ch7 << std::endl;
    fh << val << "\t" << ch7 << "\t" << AMACTime::getTime() << std::endl;
  }
  fh.close();
  return 0;
}
